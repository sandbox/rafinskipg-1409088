<?php

/**
 * @file
 * Provides a new box-type that provides multilingual box
 */

/**
 * Multilingual box.
 */
class multilanguage_boxes extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
  
	$languages = language_list('language');
    $options_array = array();
	foreach ($languages as $langcode => $language) {
	  $elemento = array();
	  $elemento['value'] = '';
      $options_array[$language->name] = $elemento;
	}
	
    return $options_array;
  }





  /**
   * Implementation of boxes_box::options_form().
   *
   * 
   */
  public function options_form(&$form_state) {

   $format =  filter_default_format();
    $form = array();
   
	$languages = language_list('language');

	foreach ($languages as $langcode => $language) {

		// form element 
		$form[$language->name] = array(
		  '#type' => 'text_format',
		  '#base_type' => 'textarea',
		  '#title' => $language->name,
		  '#description' => t("@language description", array('@language' => $language->name)),
		   '#format' => $format,
		  '#default_value' => $this->options[$language->name]['value'],
		);
	}
	
    return $form;
  }
  
  /**
   * implements boxes_box::option_submit()
   * 
   */
  public function options_submit($form, &$form_state) {
    
    foreach ($form['options'] as $field) {
      if (isset($form_state['input'][$field])) {
        $this->options[$field] = $form_state['input'][$field];
      }
    }
	print_r($form_state);
	die();
  }
 

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
   $languages = language_list('language');
    global $language;
	$render_array = array();
	$content= '';
    foreach ($languages as $langcode => $lang) {
	   if ($lang->name == $language->name){
	    
			$content =  check_markup( $this->options[$lang->name]['value'], $this->options[$lang->name]['format'], $langcode = '', FALSE);
		 
	   }
	}

    $box = array(
      'delta' => $this->delta, // Crucial.
      'title' => $this->title,
      'subject' => $this->title,
      'content' => $content,
      'is_empty' => FALSE,
    );
    
   
    return $box;
  }

}

